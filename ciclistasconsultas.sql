﻿USE ciclistas;
      
  -- consultas de COMBINACIONES INTERNAS DE seleccion

-- 1 listar las edades de los ciclistas(sin repetidos) --
SELECT DISTINCT edad FROM ciclista;

-- 2 listar las edades de los ciclistas del equipo Artiach --
SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' ;

-- 3 listar las edades del equipo artich Y Amore Vita --
SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo= 'Amore Vita' ;

-- 4 listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 --
SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30;
SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad<25 UNION SELECT c.dorsal FROM ciclista c WHERE c.edad>30;
SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad NOT BETWEEN 25 AND 30;

-- 5 listar los dorsales de los ciclistas cuya edad este entre 28 y 32 --
 SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 28 AND 32;

-- 6 indicame el nombre de los ciclistas que el numero de caracteres del nombre sea mayor que 8 --
 SELECT c.nombre FROM ciclista c WHERE CHAR_LENGTH (c.nombre)<10;
SELECT * FROM ciclista c WHERE DATE(1);

-- 7 listame el nombre y dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayusculas
  SELECT UPPER(c.nombre) NOMBRE, c.dorsal FROM ciclista c; 

-- 7b listar todos los ciclistas que han llevado el malliot amarillo mge (amarillo) en alguna etapa
  SELECT DISTINCT l.dorsal FROM maillot m INNER JOIN lleva l ON m.código = l.código WHERE m.color='amarillo'; 

-- 8  indicame el numero de etapas que hay --
  SELECT COUNT(*) FROM etapa e;   

 -- 9 listar el nombre de los puertos cuya altura sea mayor que 1500
    SELECT p.nompuerto FROM puerto p WHERE p.altura>'1500';

  -- 10 listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y3000
      SELECT p.dorsal FROM puerto p WHERE p.pendiente>'8' OR p.altura BETWEEN 1800 AND 3000;

-- 10b listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y3000
       SELECT p.dorsal FROM puerto p WHERE p.pendiente>'8' AND p.altura BETWEEN 1800 AND 3000;

 -- 11 numero de ciclistas por equipo
    SELECT AVG( c.edad),  c.nomequipo, COUNT(*) numCiclistas FROM ciclista c GROUP BY c.nomequipo; 

 -- 12 númrero de etapas que ha ganado cada uno de los ciclistas
      SELECT e.dorsal, e.numetapa FROM etapa e GROUP BY e.dorsal; 

  -- 13 el dorsal de los ciclistas que hayan  ganado más de 1 etapa
      SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;  
      
  -- 14 listar edades de todos los ciclistas de Banesto
      SELECT c.nombre, c.edad FROM ciclista c WHERE c.nomequipo='Banesto'; 
      
  -- 15 listar las edades de los ciclistas que son del Banesto o de Navigare
      SELECT c.edad FROM ciclista c WHERE c.nomequipo='Banesto' OR c.nomequipo='Navigare';
      
  -- 16 listar el dorsal de los ciclistas que son del Banesto y cuya edad sea entre 25 y 32    
      SELECT c.nombre, c.dorsal  FROM ciclista c WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;
      
  -- 17 listar el dorsal de los ciclistas que son del Banesto o cuya edad sea entre 25 y 32 
      SELECT c.nombre, c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;
      
    -- 18 listar la inicial del equipo de los ciclistas cuyo nombre comience por r
      SELECT DISTINCT c.nombre, LEFT( c.nomequipo,1) FROM ciclista c WHERE c.nombre LIKE 'R%';  
     
    -- 19 listar el código de las etapas que su salida y llegada sea en la misma población
      -- esta esta mal
      SELECT e.salida, e.llegada,e.numetapa FROM etapa e GROUP BY e.salida= e.numetapa AND e.llegada= e.numetapa;
      -- esta esta bien
      SELECT e.salida, e.llegada, e.numetapa FROM etapa e WHERE e.salida= e.llegada; 
      
    -- 20  cuantos ciclistas han ganado mas de dos etapas
      SELECT c.nombre  FROM   (SELECT e.dorsal  FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>2) c1 JOIN ciclista c USING (dorsal);  
      
    -- 21 el nombre del ciclista que tiene más edad
      SELECT * FROM ciclista c;                
      -- c1 : edad maxima
        SELECT MAX( c.edad) maxima FROM ciclista c;
        -- solucion : join
          SELECT (c.nombre) CiclistaDe, (c.edad) MáximaEdad FROM ciclista c JOIN (SELECT MAX( c.edad) maxima FROM ciclista c) c1 ON c1.maxima= c.edad;    
      
       -- 22 edad media de los ciclistas por cada equipo
        SELECT AVG( c.edad) EdadMedia, c.nomequipo FROM ciclista c GROUP BY c.nomequipo ; 
      
        -- 23 numero de ciclistas por equipo
          SELECT c.nomequipo, COUNT(*) FROM ciclista c GROUP BY c.nomequipo; 

       -- 24 numero de puertos
        SELECT COUNT(*) FROM puerto p; 

       -- 25 numero total de puertos mayores de 1500 metros
        SELECT COUNT(*) FROM  puerto p WHERE p.altura>1500;
        
      -- 26 listar el nombre de los equipos que tengan mas de 4 ciclistas
        SELECT c.NomEquipo, COUNT(*) NumCiclistas FROM   ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;
        -- con alias
        SELECT COUNT(*) nciclistas, c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING nciclistas>4;
        
        -- 27 listar el nombre de los equipos que tengan mas de 4 ciclistas cuya edad este entre 28 y 32 años
SELECT c.nombre, c.edad, COUNT(*) ciclistas, c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING ciclistas>4;

       -- 28 indicame el numero de etapas que ha ganado cada uno de los ciclistas
        SELECT e.dorsal, COUNT(*) EtapaGanada FROM  etapa e GROUP BY e.dorsal; 
        
        -- 29 indicame el dorsal de los ciclistas que hayan ganado mas de una etapa
          SELECT COUNT(*) ganadas , e.dorsal FROM etapa e GROUP BY e.dorsal HAVING ganadas>1;
           
      -- CONSULTAS DE COMBINACIONES INTERNAS DE SELECCION NUMERO 3
            
      -- 30 edades de todos los ciclistas
        SELECT c.edad FROM ciclista c;
        
      -- 31 edades de ciclistas de banesto o navigare
        SELECT c.edad, c.nomequipo FROM ciclista c WHERE c.nomequipo= 'Banesto' OR c.nomequipo= 'Navigare';
        
      -- 32 listar el dorsal de los ciclistas que son del banesto y cuya edad está entre 25 y 32
        SELECT c.dorsal, c.edad FROM ciclista c  WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32; 
        
     -- 33 listar la inicial del equipo cuyos nombres de los ciclistas al que pertenecen empiecen por r
      SELECT DISTINCT LEFT( c.nomequipo,1), c.nombre FROM ciclista c WHERE c.nombre LIKE 'r%' ;
      
      -- 34 listar el codigo de las etapas que su salida y llegada sea en la misma poblacion
        SELECT e.numetapa, e.salida, e.llegada FROM etapa e WHERE e.salida= e.llegada;
        
     -- 35 listar el codigo de las etapas que su salida y llegada no sean en la misma poblacion y 
         -- que conozcamos el dorsal del ciclista que ha ganado la etapa 
      SELECT e.numetapa, e.dorsal FROM etapa e WHERE e.salida <> e.llegada AND e.dorsal IS NOT NULL; 
      
      -- 36 listar el nombre de los puertos cuya altura este entre 1000 y 2000 o que la altura sea mayor que 2400
         SELECT p.nompuerto, p.altura FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400;
        
     -- 37 listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura este entre 1000 y 2000
      -- o que la altura sea mayor que 1400
      SELECT p.dorsal, p.nompuerto, p.altura FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400; 
      
    -- 38 listar el numero de ciclistas que hayan ganado alguna etapa
      SELECT COUNT( DISTINCT e.dorsal) ciclistasGanadores FROM etapa e;
      -- con subconsulta 
      c1 SELECT DISTINCT e.dorsal FROM etapa e ;                           
         SELECT  COUNT(*) ciclistasGanadores FROM (SELECT DISTINCT e.dorsal FROM etapa e) AS c1; 

     -- 39 listar el numero de etapas que tenga el puerto
      SELECT DISTINCT p.numetapa FROM puerto p; 

    -- 40 listar el numero de ciclistas que hayan ganado algun puerto
    -- con subconsulta
      c1 SELECT DISTINCT p.dorsal FROM puerto p; 
      SELECT COUNT(*) FROM (SELECT DISTINCT p.dorsal FROM puerto p) c1;

   -- 41 listar codigo de la etapa con el numero de puertos que tiene
     SELECT p.numetapa,COUNT(*) puerto FROM  puerto p GROUP BY p.numetapa;
    
  -- 42 indicar la altura media de los puertos
    SELECT AVG( p.altura) FROM puerto p; 
    
  -- 43 indicar el codigo de etapa cuya altura media de sus puertos esta por encima de 1500
    SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500 ;
    
  -- 44 indicar el numero de etapas que cumplen la condicion anterior
          c1   SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500 ;
          SELECT COUNT(*) FROM ( SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500) c1;

  -- 45 listar el dorsal del ciclista con el numero de veces que ha llevado algun millot
    SELECT l.dorsal, COUNT(*) numeroDeVeces FROM lleva l GROUP BY l.dorsal, l.código; 

  -- 46 listar el dorsal, el numero de etapa, el ciclista y el numero de maillots que ese ciclista ha llevado en cada etapa
   c1 SELECT l.dorsal, l.numetapa, COUNT(*) maillots FROM lleva l GROUP BY l.dorsal, l.numetapa;
   SELECT c.nombre, c1.* FROM (SELECT l.dorsal, l.numetapa, COUNT(*) maillots FROM lleva l GROUP BY l.dorsal, l.numetapa) c1 INNER JOIN ciclista c ON c1.dorsal= c.dorsal;

    -- CONSULTAS de combinaciones internas DE SELECCION NUMERO 4

  -- 47 nombre y edad de los ciclistas que han ganado etapas
    c1 SELECT c.nombre, c.edad FROM ciclista c;   
SELECT c1.*, e.numetapa FROM (SELECT c.nombre, c.edad, c.dorsal FROM ciclista c) c1 INNER JOIN etapa e ON c1.dorsal= e.dorsal;

-- 48 nombre y edad de los ciclistas que han ganado puertos  
SELECT DISTINCT nombre, edad FROM ciclista c INNER JOIN puerto p ON  p.dorsal = c.dorsal;

-- 49 nombre y edad de los ciclistas que han ganado etapas y puertos
  SELECT DISTINCT nombre, edad FROM(ciclista INNER JOIN etapa e ON ciclista.dorsal = e.dorsal) INNER JOIN puerto p ON ciclista.dorsal = p.dorsal;   

-- 50 listar el director de los equpos que tengan ciclistas que hayan ganado alguna etapa
  SELECT DISTINCT e.director FROM equipo e INNER JOIN (ciclista c INNER JOIN etapa e1 ON c.dorsal = e1.dorsal) ON e.nomequipo= c.nomequipo;
  
-- 51 dorsal y nombre de los ciclistas que hayan llevado algun maillot
  SELECT DISTINCT c.dorsal, c.nombre FROM ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal; 
  SELECT DISTINCT c.dorsal, c.nombre  FROM (ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) INNER JOIN maillot m ON l.código = m.código;
  
-- 52  dorsal y nombre de los ciclistas que hayan llevado algun maillot amarillo
  SELECT DISTINCT c.dorsal, c.nombre FROM (ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) INNER JOIN maillot m ON l.código = m.código WHERE m.color='amarillo';
  SELECT DISTINCT c.dorsal, c.nombre FROM maillot m INNER JOIN(ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) ON m.código= l.código WHERE m.color='amarillo';
      
 -- 53 dorsal de los ciclistas que hayan llevado algun maillot y que han ganado etapas
  c1 SELECT DISTINCT e.dorsal FROM (etapa e JOIN lleva l ON e.dorsal = l.dorsal) JOIN maillot m ON l.código = m.código; 
-- para poder meter la tabla ciclista y asi meter el nombre, etc.
  SELECT DISTINCT c1.* FROM (SELECT DISTINCT e.dorsal FROM (etapa e JOIN lleva l ON e.dorsal = l.dorsal) JOIN maillot m ON l.código = m.código) c1 JOIN ciclista c ON c.dorsal= c1.dorsal;    

-- 54 indicar el numero de etapa de las etapas que tengan puertos
  -- haciendo join
  SELECT DISTINCT e.numetapa FROM etapa e INNER JOIN puerto p USING (numetapa) WHERE e.numetapa= p.numetapa;
-- sin hacer join y listando las etapas que son
 c1 SELECT DISTINCT p.numetapa FROM puerto p;
SELECT COUNT(*)  FROM(SELECT DISTINCT p.numetapa FROM puerto p) c1;

-- 55 indicar los kilometros de las etapas que hayan ganado ciclistas del banesto y que tengan puertos
 SELECT e.kms, e.numetapa, c.nombre FROM (ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal) 
INNER JOIN puerto p ON e.numetapa = p.numetapa WHERE c.nomequipo='Banesto';

-- 56 listar el numero de ciclistas que hayan ganado alguna etapa con puerto
c1  SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa = p.numetapa;   
        SELECT COUNT( c1.dorsal) FROM (SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa = p.numetapa) c1 ;
        
-- 57 indicar el nombre de los puertos que hayan sido ganados por ciclistas del banesto
  SELECT p.nompuerto, c.nomequipo, c.nombre FROM puerto p INNER JOIN ciclista c ON p.dorsal = c.dorsal WHERE c.nomequipo='Banesto'; 
  
-- 58 listar el numero de etapas que tengan puerto, que hayan sido ganados por ciclistas del banesto con mas de 200 km
 SELECT DISTINCT p.numetapa, c.nombre, e.kms, p.nompuerto, c.dorsal FROM (puerto p INNER JOIN etapa e ON p.numetapa = e.numetapa)
INNER JOIN ciclista c ON p.dorsal = c.dorsal WHERE c.nomequipo='Banesto' AND e.kms>100;

       -- consultas externas de seleccion numero 5

-- 59 nombre y edad de los ciclistas que no han ganado etapas
   SELECT DISTINCT c.nombre, c.edad FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL;
  
-- 60 nombre y edad de los ciclistas que no han ganado puertos
  SELECT c.nombre, c.edad FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal WHERE p.dorsal IS NULL;
  
-- 61 listar el director de los equipos que tengan ciclistas que no hayan ganado ninguna etapa
  SELECT DISTINCT director FROM equipo INNER JOIN (ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal)
  ON equipo.nomequipo= c.nomequipo WHERE e.dorsal IS NULL;  
  
-- 62 dorsal y nombre de los ciclistas que no hayan llevado algún maillot
  SELECT * FROM
    
--                                         







      -- SELECT UPPER(left('astur',2));    
